#include <iostream>
 
#include "Client.h"
 
using namespace std; 
Client::Client(){
    serverAddr.sin_family = PF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
    sock = 0;
    pid = 0;
    isClientwork = true;
    epfd = 0;
}
 
void Client::Connect() {
    cout << "Connect Server: " << SERVER_IP << " : " << SERVER_PORT << endl;
    
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("sock error");
        exit(-1); 
    }
 
    if(connect(sock, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) {
        perror("connect error");
        exit(-1);
    }
 
    // 创建管道，其中fd[0]用于父进程读，fd[1]用于子进程写
    if(pipe(pipe_fd) < 0) {
        perror("pipe error");
        exit(-1);
    }
 
    epfd = epoll_create(EPOLL_SIZE);
    
    if(epfd < 0) {
        perror("epfd error");
        exit(-1); 
    }
 
    addfd(epfd, sock, true);
    addfd(epfd, pipe_fd[0], true);
 
}
 
// 断开连接，清理并关闭文件描述符
void Client::Close() {
 
    if(pid){
       //关闭父进程的管道和sock
        close(pipe_fd[0]);
        close(sock);
    }else{
        //关闭子进程的管道
        close(pipe_fd[1]);
    }
}
 
void Client::Start() {
 
    static struct epoll_event events[2];

    Connect();
    pid = fork();
    
    if(pid < 0) {
        perror("fork error");
        close(sock);
        exit(-1);
    } else if(pid == 0) {
        // 进入子进程执行流程
        close(pipe_fd[0]); 
 
        // 输入exit可以退出聊天室
        cout << "Please input 'exit' to exit the chat room" << endl;
        cout<<"\\ + ClientID to private chat "<<endl;
        // 如果客户端运行正常则不断读取输入发送给服务端
        while(isClientwork){
            memset(msg.content,0,sizeof(msg.content));
            fgets(msg.content, BUF_SIZE, stdin);
            if(strncasecmp(msg.content, EXIT, strlen(EXIT)) == 0){
                isClientwork = 0;
            }
            // 子进程将信息写入管道
            else {
                memset(send_buf,0,BUF_SIZE);
                memcpy(send_buf,&msg,sizeof(msg));
                if( write(pipe_fd[1], send_buf, sizeof(send_buf)) < 0 ) { 
                    perror("fork error");
                    exit(-1);
                }
            }
        }
    } else { 
        //pid > 0 父进程
        close(pipe_fd[1]); 
 
        while(isClientwork) {
            int epoll_events_count = epoll_wait( epfd, events, 2, -1 );
 
            for(int i = 0; i < epoll_events_count ; ++i)
            {
                memset(recv_buf,0,sizeof(recv_buf));
                if(events[i].data.fd == sock)
                {
                    int ret = recv(sock, recv_buf, BUF_SIZE, 0);
                    memset(&msg,0,sizeof(msg));
                    memcpy(&msg,recv_buf,sizeof(msg));
 
                    if(ret == 0) {
                        cout << "Server closed connection: " << sock << endl;
                        close(sock);
                        isClientwork = 0;
                    } else {
                        cout << msg.content << endl;
                    }
                }
                else { 
                    int ret = read(events[i].data.fd, recv_buf, BUF_SIZE);
                    if(ret == 0)
                        isClientwork = 0;
                    else {
                        send(sock, recv_buf, sizeof(recv_buf), 0);
                    }
                }
            }//for
        }//while
    }
    
    Close();
}